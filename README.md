# mailing sender

* [Установка](#установка)

## Устанвока

### Шаг 1

Если у вас нет Python, то установите его по [ссылке](https://www.python.org/downloads/)

### Шаг 2

Создаём виртальное окружение внутри папки с скачанным проектом

#### Unix/macOs

```
python3 -m venv env
```

#### Windows

```
py -m venv env
```

### Шаг 3

Активация виртуального окружения

#### Unix/macOs

```
source env/bin/activate
```

#### Windows

```
.\env\Scripts\activate
```

### Шаг 4

Установка дополнительных библиотек

```
pip install -r requirements.txt
```

### Шаг 5

Принимаем все миграции для базы данных (внутри проекта лежит файлик manage.py)

#### Unix/macOs

```
python3 manage.py migrate
```

#### Windows

```
py manage.py migrate
```

### Шаг 6

установка redis:
https://redis.io/docs/getting-started/

### Шаг 7

Запуск проекта

#### Unix/macOs

```
python3 manage.py runserver
```

#### Windows

```
py manage.py runserver
```

### Шаг 8

запуск redis сервера

выполнять в новой консоли

```
redis-server
```

### Шаг 9

запуск celery worker

выполнять в новой консоли

```
cd mailing_list
celery -A mailing_lists worker -l info
```

### Шаг 10

запуск ежеминутной проверки на наличие рассылок

выполнять в новой консоли

```
cd mailing_list
celery -A mailing_lists beat -l info
```

Приятного использования