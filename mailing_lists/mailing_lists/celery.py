import os

from celery import Celery
from .schedule import CELERYBEAT_SCHEDULE

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "mailing_lists.settings")

app = Celery("mailing_lists")
app.config_from_object("django.conf:settings", namespace="CELERY")
app.autodiscover_tasks()
app.conf.beat_schedule = CELERYBEAT_SCHEDULE
