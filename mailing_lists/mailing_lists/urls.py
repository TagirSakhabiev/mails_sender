from client.views import ClientViewSet
from django.contrib import admin
from django.urls import path, include
from mailingLists.views import MailingListViewSet
from rest_framework.routers import SimpleRouter

maillists_router = SimpleRouter()
maillists_router.register(r"mailinglists", MailingListViewSet)
client_router = SimpleRouter()
client_router.register(r"clients", ClientViewSet)
urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/v1/', include(maillists_router.urls + client_router.urls))
]
