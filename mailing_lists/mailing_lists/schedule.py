from celery.schedules import crontab

CELERYBEAT_SCHEDULE = {
    'send_all_mails': {
        'task': 'send_all_mails',
        'schedule': crontab(minute='*/1'),
    }}
