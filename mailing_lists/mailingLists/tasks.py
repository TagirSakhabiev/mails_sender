from datetime import datetime
from time import sleep as sl

import requests
from client.models import Client
from django.db.models import Q
from mailing_lists.celery import app as celery_app
from requests.structures import CaseInsensitiveDict

from .models import MailingList, Mail


def send_mail(id, phone, text):
    url = f"https://probe.fbrq.cloud/v1/send/{id}"
    headers = CaseInsensitiveDict()
    headers["accept"] = "application/json"
    headers[
        "Authorization"] = "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2OTUxMDkzMDQsImlzcyI6ImZhYnJpcXVlIiwibmFtZSI6ImtpbGxyb3lrYSJ9.SpMkh_lkv5rYsK2UhvIGYkZkv6XexN4qNNroLkFV5ho"
    headers["Content-Type"] = "application/json"
    data = "{" + f"""
      "id": {id},
      "phone": {phone},
      "text": "{text}"
    """ + "}"
    resp = requests.post(url, headers=headers, data=data)
    return resp


@celery_app.task
def do_smth():
    sl(5)


@celery_app.task(name="send_all_mails")
def send_all_mails():
    time_now = datetime.now().time()
    need_to_send = MailingList.objects.filter(time_start__lt=time_now, time_end__gt=time_now)
    mails = Mail.objects.all()
    if need_to_send:
        for mailinglist in need_to_send:
            try:
                users = Client.objects.filter(phone_code=mailinglist.filter)
            except Exception:
                users = Client.objects.filter(tag=mailinglist.filter)
            for user in users:
                if not mails.filter(Q(mailing_list=mailinglist) & Q(client=user)):
                    mail = Mail(
                        send_time=time_now,
                        status=True,
                        mailing_list=mailinglist,
                        client=user
                    )
                    status = send_mail(mailinglist.id, user.phone_number, mailinglist.text).status_code
                    if status == 200:
                        mail.status = True
                    else:
                        mail.status = False
                    mail.save()
                not_sended_mails = \
                    mails.filter(mailing_list=mailinglist, client=user, status=False).prefetch_related("client")
                if not_sended_mails:
                    for mail in not_sended_mails:
                        status = send_mail(mail.id, mail.client.phone_number, mailinglist.text).status_code
                        if status == 200:
                            mail.status = True
                        else:
                            mail.status = False
                        mail.save()
