from django.contrib import admin

from .models import MailingList, Mail


@admin.register(MailingList)
class MailingListAdmin(admin.ModelAdmin):
    pass


@admin.register(Mail)
class MailAdmin(admin.ModelAdmin):
    pass
