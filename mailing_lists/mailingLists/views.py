from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet

from .models import MailingList
from .serializers import MailingListSerializer, MailSerializer


class MailingListViewSet(ModelViewSet):
    queryset = MailingList.objects.all()
    serializer_class = MailingListSerializer

    @action(detail=True)
    def get_statistic(self, request, pk=None):
        mailing_list = MailingList.objects.get(id=pk)
        serialazer = MailingListSerializer(mailing_list)
        answer = {
            "mailinglist": serialazer.data,
            "mails_count": mailing_list.mails.count(),
            "successful_mails_count": mailing_list.mails.filter(status=True).count(),
            "unsuccessful_mails_count": mailing_list.mails.filter(status=False).count(),
        }
        return Response(answer)

    @action(detail=False)
    def get_all_statistic(self, request):
        mailing_list = MailingList.objects.all().prefetch_related("mails")
        answer = {
            "mailing_list": []
        }
        for x in mailing_list:
            answer["mailing_list"].append(MailingListSerializer(x).data)
            answer["mailing_list"][-1]["mails"] = MailSerializer(x.mails.all(), many=True).data

        return Response(answer)
