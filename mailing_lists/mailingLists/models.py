from django.db import models


# Create your models here.
class MailingList(models.Model):
    time_start = models.TimeField()
    text = models.TextField(null=True)
    filter = models.TextField(null=True, max_length=255)
    time_end = models.TimeField(null=True)


class Mail(models.Model):
    send_time = models.TimeField(auto_created=True)
    status = models.BooleanField(default=False)
    mailing_list = models.ForeignKey("MailingList", on_delete=models.DO_NOTHING, blank=True, null=True,
                                     related_name="mails")
    client = models.ForeignKey("client.Client", on_delete=models.DO_NOTHING, blank=True, null=True)
