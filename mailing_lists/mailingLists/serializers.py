from rest_framework import serializers

from .models import MailingList, Mail


class MailingListSerializer(serializers.ModelSerializer):
    class Meta:
        model = MailingList
        fields = "__all__"
        read_only_fields = ["id"]


class MailSerializer(serializers.ModelSerializer):
    class Meta:
        model = Mail
        fields = "__all__"
        read_only_fields = ["id"]
