from django.db import models


# Create your models here.
class Client(models.Model):
    phone_number = models.IntegerField()
    phone_code = models.IntegerField()
    tag = models.TextField()
    time_zone = models.TextField(max_length=30)
