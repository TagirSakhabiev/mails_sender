# Generated by Django 3.2 on 2022-09-20 07:30

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('client', '0003_alter_client_time_zone'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='client',
            name='mails',
        ),
        migrations.RemoveField(
            model_name='client',
            name='tag',
        ),
        migrations.AddField(
            model_name='client',
            name='tag',
            field=models.ManyToManyField(to='client.Tag'),
        ),
    ]
